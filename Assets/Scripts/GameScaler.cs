﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScaler : MonoBehaviour
{
    float aspect;
    [SerializeField]float normAspect = 0.5625f;
    [SerializeField]Transform lvlup;
    [SerializeField]List<RectTransform> Canvas2Align;
    public static float factor;
    Camera cam;

    void Awake()
    {
        cam = Camera.main;
        Scale();
    }

    void Scale(){
        aspect = cam.aspect;
        factor =1;
        if(aspect<normAspect){
            factor = aspect/normAspect;
            // Vector3 pos = cam.ScreenToWorldPoint(new Vector2 (cam.pixelWidth/2f,cam.pixelHeight));
            // Debug.Log(pos);
            foreach(var canvas in Canvas2Align){
                Vector3 pos = canvas.localPosition;
                canvas.localPosition=new Vector3(pos.x,pos.y/factor,pos.z);
            }
        }
        transform.localScale = Vector3.one*factor;
        if(aspect>normAspect){
            lvlup.localScale *=aspect/normAspect;
            // lvlup.localPosition+= Vector3.down*4.2f;
        }

    }
}
