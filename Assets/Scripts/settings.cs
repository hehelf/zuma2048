﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class settings : MonoBehaviour
{
    [SerializeField]GameManager GM;
    [SerializeField]InputField lvl;
    [SerializeField]InputField flight;
    [SerializeField]InputField speed;
    [SerializeField]InputField different;
    [SerializeField]InputField number;
    [SerializeField]InputField map;
    public void GetSettings(){
            lvl.text = GM.level.ToString();
         
            speed.text = GM.BallsSpeed.ToString();
            different.text = GM.numberOfDifferentBalls.ToString();
            number.text = GM.BallsNumber.ToString();
            map.text = GM.mapNum.ToString();
            flight.text = GM.BallFlightSpeed.ToString();

    }
    public void SetSettings(){
        GM.level = int.Parse(lvl.text);
        GM.BallFlightSpeed = int.Parse(flight.text);
        GM.BallsSpeed = int.Parse(speed.text);
        GM.numberOfDifferentBalls = int.Parse(different.text);
        GM.mapNum = int.Parse(map.text);
        GM.BallsNumber = int.Parse(number.text);

    }
}
