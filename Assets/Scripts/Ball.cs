﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class Ball : MonoBehaviour
{

   public int value;
   public bool Current = false;
   public Vector3 tangent;
    [SerializeField]List<Color> BallColor;
    [SerializeField]Texture BallGradient;
   GameManager GM;
   	private MovementPath path;
    TextMeshPro textMesh;

    ParticleSystem goBackParticle;
    public GameObject FinalTrail;

   
       void Start()
    {
        GM = GetComponentInParent<GameManager>();
        textMesh = GetComponentInChildren<TextMeshPro>();
        UpdValue();

		path = GM.GetComponentInChildren<MovementPath>();
        goBackParticle = GetComponentInChildren<ParticleSystem>();
        goBackParticle.Stop();
    }

    void FixedUpdate()
    {
       if(Current){SetVelocity();}
       
    }
    void Update(){

        
        if(gameObject.tag=="ActiveBalls" ){
            if(goBackParticle.isStopped&&MovementPath.GOBack){
            goBackParticle.Play();
        }
                if(goBackParticle.isPlaying&&!MovementPath.GOBack){
            goBackParticle.Stop();
        }
        if(MovementPath.final){
            if(FinalTrail==null){
            FinalTrail = Instantiate(GM.Trail,transform.position,Quaternion.Euler (90,0,0),transform.parent);
            // FinalTrail.transform.localRotation = Quaternion.Euler (0,90,90);
            FinalTrail.transform.localScale=Vector3.one*path.ballRadius*FinalTrail.transform.localScale.x;
            }
            FinalTrail.transform.position = transform.position;
            // FinalTrail.transform.rotation = Quaternion.Euler(tangent);
        }

        }

    }

    public void UpdValue(){
            GetComponent<MeshRenderer>().material.color = Color.white;
            GetComponent<MeshRenderer>().material.mainTexture = null;
                int i = (value-1)%11;
                if (i<10){
                    GetComponent<MeshRenderer>().material.color = BallColor[i];
                }else{
                    GetComponent<MeshRenderer>().material.mainTexture = BallGradient;
                }             

                textMesh.text = StrigByValue(value);
 
    }

    public static string StrigByValue(int value){
        string str;
            if(value<12){str = Mathf.Pow(2,value).ToString();}
        else {
        if(value<21)str = Mathf.Pow(2,value -10).ToString()+"k";
            else if(value<30)str = Mathf.Pow(2,value -20).ToString()+"m";
                else str = Mathf.Pow(2,value -30).ToString()+"t";
        }
        return str; 
    }
        void SetVelocity(){
            transform.position += new Vector3(0,0,GM.BallFlightSpeed*0.1f);
            if(transform.position.magnitude>GM.FlightLength){
                Destroy(gameObject);
                
                    }
        }

    void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "ActiveBalls" && Current&& gameObject.tag == "NewBall")
		{
			Current = false;
            
			this.gameObject.tag = "ActiveBalls";
			// this.gameObject.layer = LayerMask.NameToLayer("ActiveBalls");

			ContactPoint contact = other.contacts[0];
			Vector3 CollisionDir = contact.point - other.transform.position;

			int currentIdx = other.transform.GetSiblingIndex();

			float angle  = Vector3.Angle(CollisionDir, other.gameObject.GetComponent<Ball>().tangent);
			if ( angle > 90)
				path.AddNewBallAt(this.gameObject, currentIdx + 1, currentIdx);
			else
				path.AddNewBallAt(this.gameObject, currentIdx, currentIdx);

			// this.gameObject.GetComponent<BallCollider>().enabled = false;
		}
            if(other.gameObject.tag == "ActiveBalls" && gameObject.tag == "FinalBall"){
                PlayerPrefs.SetInt("inProgress",0);
                PlayerPrefs.Save();

               if(MovementPath.final){
                   Destroy(other.gameObject);
                   Ball otherBall = other.gameObject.GetComponent<Ball>();
                   otherBall.FinalTrail.transform.position = transform.position;
                    Destroy(otherBall.FinalTrail,1);
                    MovementPath.final = false;
                    value++;
                    GameManager.GameStopped = true;
                    UpdValue();
                    GM.SummonFinalPuf(transform);
                }else   GM.final(false);;  
            }
	}

        
}
