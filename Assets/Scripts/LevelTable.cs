using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelTable{
    public OneLevel[] levelTable;
    [System.Serializable]public class OneLevel{
        public int n;
        public int exp;
    }
}