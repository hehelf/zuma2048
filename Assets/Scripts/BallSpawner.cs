﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;

public class BallSpawner : MonoBehaviour
{
   GameManager GM;

//    [SerializeField]List <Vector3> ColorDistribution;
   MovementPath path;
    Vector3 spawnPos;

    public int prevVaule;
    [SerializeField]Material finalMat;
    Quaternion ballRotate = Quaternion.Euler(new Vector3(90,0,90));
    // Quaternion ballRotate = Quaternion.Euler(Vector3.zero);


    void Start()
    {   
        prevVaule = -1;
        GM = GetComponentInParent<GameManager>();
        path = GetComponent<MovementPath>(); 
        spawnPos = GetComponent<BGCurve>().Points[0].PositionWorld;
         
        if(PlayerPrefs.HasKey("PathProgress")&&PlayerPrefs.HasKey("inProgress")&&PlayerPrefs.GetInt("inProgress")==1){
           GM.LoadLevelProgress();
        }else {
           SpawnBalls(GM.BallsNumber);}
        
         SpawnFinalBall();
    }

     public void SpawnBalls(int i){

        for (int j = 0; j < i; j++)
                {
                GameObject ball =  SpawnBall(spawnPos);
                
                    ball.tag = "ActiveBalls";

                    
                }
        
    }

    GameObject SpawnBall(Vector3 pos){

        GameObject ball = Instantiate(GM.BallPrefab,pos,ballRotate,transform);

        int min = GM.powOfWin+GM.level-GM.numberOfDifferentBalls;
            min = min<1?1:min;
        
        // int max = min+3;
        int val;
        do
        {
            val = RandomBall(min);
        } while (val == prevVaule); 
        
        ball.GetComponent<Ball>().value = val;
        prevVaule = val;
        return ball;
    }
    int RandomBall(int min){
         int value = 0;
        float rnd =  Random.Range(0.0f, 1.0f);
        if(rnd<=0.15f) value  = min; 
        if(rnd>0.15f&&rnd<=0.5f) value  = min+1; 
        if(rnd>0.5f&&rnd<=0.85f) value  = min+2; 
        if(rnd>0.85f) value  = min+3; 
        return value;
    }
    
    void SpawnFinalBall(){
        BGCurve bg = GetComponent<BGCurve>();
        BGCcMath math = GetComponent<BGCcMath>();
        int cnt=bg.PointsCount;

        Vector3 pos = bg.Points[cnt-1].PositionWorld + Vector3.up*0.01f;
        GameObject ball = Instantiate(GM.BallPrefab,pos,ballRotate,transform);
        ball.GetComponent<MeshRenderer>().material =finalMat;
        ball.GetComponent<Ball>().value = GM.powOfWin+GM.level;
         ball.tag = "FinalBall";
    }
    public void SpawnSavedBalls(float headDist, List<int> PathBalls){
        int len = PathBalls.Count;
        for (int j = 0; j < len; j++)
                {
                    float currentBallDist = headDist - j * path.ballRadius;
                    Vector3 pos = GetComponent<BGCcMath>().CalcPositionByDistance(currentBallDist);

                    GameObject ball = Instantiate(GM.BallPrefab,pos,ballRotate,transform);
                    ball.GetComponent<Ball>().value = PathBalls[j];
                    ball.tag = "ActiveBalls";
                    
                }
    }
}
