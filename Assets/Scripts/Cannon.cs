﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;
public class Cannon : MonoBehaviour
{
    [SerializeField]Transform[] slots;
    [SerializeField]Transform[] borders;
    public List<GameObject> Balls = new List<GameObject>();

    [SerializeField]BGCurve cycle;
    [SerializeField]float ShootingDelay;
    [SerializeField]Animator GunAnim;
    
    private bool prevGameStopped;
    public float duration ;

    bool MouseDown = false;
   GameManager GM;
    Quaternion ballRotate = Quaternion.Euler(new Vector3(90,0,90));
    List<float> bordPosX = new List<float>();
    bool blow;
    bool ShootAllow;
    
    
    public bool switchBall;

    void Start()
    {
        // CurrentTapPos = -10;
        GM = GetComponentInParent<GameManager>();
     

        bordPosX.Add(-4.83f*GameScaler.factor);
        bordPosX.Add(4.83f*GameScaler.factor);

        Balls.Add(SpawnBall(slots[0].position));
        Balls.Add(SpawnBall(slots[1].position));
        switchBall=false;
        blow = false;
        ShootAllow = true;
    }

    void Update()
    {
                if(switchBall){
                    if(Balls[0]!=null)FollowBall(Balls[0].transform,1);
                    if(Balls[1]!=null)FollowBall(Balls[1].transform,0);
                }

                if(blow)
                    if(Balls[1].GetComponent<Animation>().isPlaying){
                    
                }else{
                    if(blow)blow=false;
                    Balls[1].GetComponent<Animation>().enabled=false;
                }

        if(GameManager.GameStopped)
        {
           if(!prevGameStopped)prevGameStopped=true;
            return;
        }
            if(prevGameStopped){
                prevGameStopped=false;
                StartCoroutine(ShootCD(ShootingDelay));
            }

            Vector3 cursor = Input.mousePosition;
            cursor = Camera.main.ScreenToWorldPoint(cursor);
            // Debug.Log(bordPosX[0]+"   "+bordPosX[1]+"   "+cursor);
            if(Input.GetMouseButton(0)){
                float xPos = cursor.x;
                if (cursor.x<bordPosX[0])xPos = bordPosX[0];
                if (cursor.x>bordPosX[1])xPos = bordPosX[1];
                
                transform.DOMove(new Vector3( xPos,transform.position.y,transform.position.z),duration);
            }
            

            if(MovementPath.final) return;
   
           if(Input.GetMouseButtonUp(0)&&ShootAllow&&!blow){
            Shoot();

             }   
            //  if(!ShootAllow)ShootAllow=true;       

    }

    void FixedUpdate(){

    }
    public void UpdBallsInCannon(){
        if(Balls[0]!=null)Destroy(Balls[0]);
        if(Balls[1]!=null)Destroy(Balls[1]);
        Balls[0] = (SpawnBall(slots[0].position));
        Balls[1]= (SpawnBall(slots[1].position));
    }


        void Shoot(){
             if(GameManager.GameStopped||MovementPath.final) return;
                // Balls[0].transform.position = ShootStartPos.position;
                Balls[0].GetComponent<Ball>().Current = true;
                Balls[0].transform.parent = GetComponentInParent<GameManager>().transform;
                Balls[0].tag = "NewBall";
            Balls[0]=Balls[1];
            GunAnim.SetTrigger("shot");
            switchBall = true;
            StartCoroutine(ShootCD(ShootingDelay));
            // Balls[0].transform.position = slots[0].position;
            Balls[1]=null;
        }


        
    GameObject SpawnBall(Vector3 pos){
        GameObject ball = Instantiate(GM.BallPrefab,pos,ballRotate,transform);
                int min = GM.powOfWin+GM.level-GM.numberOfDifferentBalls;
            min = min<1?1:min;
        
        ball.GetComponent<Ball>().value = RandomBall(min);
        return ball;
    }

    // void BlowBall(){
    //     Balls[1]= (SpawnBall(slots[1].position));
    // }
        int RandomBall(int min){
        int value = 0;
        float rnd =  Random.Range(0.0f, 1.0f);
        if(rnd<=0.15f) value  = min; 
        if(rnd>0.15f&&rnd<=0.5f) value  = min+1; 
        if(rnd>0.5f&&rnd<=0.85f) value  = min+2; 
        if(rnd>0.85f) value  = min+3; 
        return value;
    }
    public void SetBallsInCannon(List<int> vals){
        Balls[0].GetComponent<Ball>().value = vals[0];
        Balls[1].GetComponent<Ball>().value = vals[1];
    }


    void FollowBall(Transform ball,int i){
        ball.position = slots[i].position;
    }
    
    public void SpawnNewBall(){
        Balls[1]=SpawnBall(slots[0].position);
        Balls[1].GetComponent<Animation>().enabled = true;
        blow = true;

    }
    
    public void RenameNewBall(){
           if(Balls.Count<=1) return;
        //    Balls[1].name = "BallInCannon"; 
    }
    IEnumerator ShootCD(float sec){
        ShootAllow = false;
        float tim = Time.time;
        yield return new WaitForSeconds(sec);
        // Debug.Log(tim-Time.time);
         ShootAllow = true;
    }

}
