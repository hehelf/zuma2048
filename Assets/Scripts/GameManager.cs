﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;
using UnityEngine.UI;
using DG.Tweening;
using GameAnalyticsSDK;
using TMPro;
public class GameManager : MonoBehaviour
{
    public int level;
    public int powOfWin;
    public float BallFlightSpeed;
    public float FlightLength;
    public int numberOfDifferentBalls;
    public int mapNum;
        [SerializeField]int totalMaps;
    [SerializeField]float UnderlineOffset;

    public GameObject BallPrefab;
    public bool UpdColorOn;
    public static bool GameStopped = false;
    public GameObject PathPrefab;
     [SerializeField]GameObject finScreen;
      [SerializeField]List<TextMeshProUGUI> finText;
     [SerializeField]GameObject nextBtn;
     [SerializeField]GameObject restartBtn;

    public float BallsSpeed;
    public int BallsNumber;
    [SerializeField]GameObject settingsGo;
    [SerializeField]Animation ComboAnim;
    [SerializeField]Text StartDelayText;
     [SerializeField]GameObject PlayButton;
    [Space] 
    [SerializeField]GameObject Puf;
    public GameObject Trail;
    [SerializeField]GameObject FinalPuf;
    [SerializeField]List<Sprite> ComboText;
    [SerializeField]GameObject GoBackArrows;
    SpriteRenderer ComboSpriteRenderer;
        GameObject pathGO1;
     MovementPath path;
    BallSpawner spawner;
    bool hideComb;
    Cannon cannon;
    Coroutine StartDelayCoroutine;
    bool islvlsaved;
    ScoreManager SM;
    int matchNow=0;
    public bool LVLFailed =false;

    void Awake(){
        GameAnalytics.Initialize();
    }

    void Start()
    {
        cannon =  GetComponentInChildren<Cannon>();
        SM = GetComponent<ScoreManager>();

     
        LoadGame();
    //    SpawnPath();
        if(PlayerPrefs.HasKey("inProgress")&&PlayerPrefs.GetInt("inProgress")==1){
        pause();
        }else GameStopped = false;
        hideComb = true;
        islvlsaved=false;
        
        // foreach (var item in finScreen.GetComponentsInChildren<TextMeshPro>())
        // {
        //     Debug.Log(item.text)
        // }

    }

    // Update is called once per frame
    void Update()
    {
        
        if(pathGO1==null){
            LVLFailed = false;
            SM.UpdText();
            SpawnPath();
            islvlsaved=false;
            cannon.UpdBallsInCannon();
        }
     if(GameManager.GameStopped) {
         
        //   if(Input.GetMouseButton(0)&&StartDelayText.gameObject.activeSelf&&StartDelayText.text==tap2start&&!settingsGo.activeSelf){
            
        //   }
         return;
     }

 
    if (matchNow!=path.matchCounter) {
        matchNow = path.matchCounter;
 
        if(matchNow>1){
            SetComboText(matchNow-2);
        }
  
    }
        //   if(MovementPath.GOBack){
        //     if(!GoBackArrows.activeSelf){
        //         GoBackArrows.SetActive(true);

        //           Debug.Log("show");
        //     }
        // }else if(GoBackArrows.activeSelf){
        //     Debug.Log("hide");
        //         GoBackArrows.SetActive(false);
        //     }
       
    }

    public void Play(){
        PlayButton.SetActive(false);
        StartDelayCoroutine = StartCoroutine(StartDelay());  
    }

    void SetComboText(int i){
        if(i>=2) i = Random.Range(2,5);
        
         ComboAnim.gameObject.SetActive(false);

        ComboAnim.gameObject.SetActive(true);
        ComboAnim.gameObject.GetComponent<SpriteRenderer>().sprite= ComboText[i];
    }

    void SpawnPath(){
        if(mapNum<1||mapNum>totalMaps){mapNum = 1;}
        PathPrefab = Resources.Load("maps/"+mapNum) as GameObject;
            pathGO1 = Instantiate(PathPrefab,Vector3.zero,Quaternion.identity,transform);
              path =  GetComponentInChildren<MovementPath>();
            spawner = GetComponentInChildren<BallSpawner>();

//желтая подводка
            Vector3 pathpos = path.transform.position;
               GameObject Underline = Instantiate(path.gameObject,new Vector3(pathpos.x,pathpos.y-2,pathpos.z-UnderlineOffset),Quaternion.identity,pathGO1.transform);
                Color undercolor;
                 ColorUtility.TryParseHtmlString("#dc9c57", out undercolor);
                 LineRenderer line= Underline.GetComponent<LineRenderer>();
                line.startColor = undercolor;
                line.endColor = undercolor;
                Underline.GetComponent<BGCurve>().enabled = false;
                Underline.GetComponent<MovementPath>().enabled = false;
                Underline.GetComponent<BallSpawner>().enabled = false;
    }

    public void final(bool win){
        DOTween.KillAll();
        GameStopped = true;
        finScreen.SetActive(true);

        //макс.шары
        // finText[1].text=Ball.StrigByValue(powOfWin+level-1);
        // finText[2].text=Ball.StrigByValue(powOfWin+level);
         Quaternion ballRotate = Quaternion.Euler(new Vector3(90,0,90));
        GameObject ball1 = Instantiate(BallPrefab,finText[1].transform.position,ballRotate,path.transform);
                ball1.GetComponent<Ball>().value = powOfWin+level-1;
                ball1.transform.localScale = Vector3.one*1.2f;
        GameObject ball2 = Instantiate(BallPrefab,finText[2].transform.position,ballRotate,path.transform);
                ball2.GetComponent<Ball>().value = powOfWin+level;
                ball2.transform.localScale = Vector3.one*1.45f;

        //скор
        finText[4].text=ScoreManager.score+"";
        finText[5].text=ScoreManager.highscore+"";

        //костыль - пряччем трубу под затемнение

        
        SpriteRenderer truba = path.transform.parent.Find("1-tpy6a").GetComponent<SpriteRenderer>();
        truba.material.renderQueue = 3000;
        
            if(win){
                finText[0].text= "STAGE CLEARED!";
                // finText[3].text=Ball.StrigByValue(powOfWin+level+1);
                        GameObject ball3 = Instantiate(BallPrefab,finText[3].transform.position,ballRotate,path.transform);
                         ball3.GetComponent<Ball>().value = powOfWin+level+1;
                        ball3.transform.localScale = Vector3.one*1.2f;

                //  nextBtn.SetActive(true);
                TryIncAndSave();
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, $"Complete level {level}");

            }else{
                LVLFailed = true;
                finText[0].text= "STAGE FAILED!";
                finText[3].text="?";
                // restartBtn.SetActive(true);  
                SM.Fail();
                SaveGame(); 
                GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, $"Fail level {level}");

            }
            
    }
    void TryIncAndSave(){
                if(!islvlsaved){
                    islvlsaved=true;
                    level++;
                    mapNum++;
                    SaveGame(); 
                }
    }
    public void pause(){
        if(settingsGo.activeSelf)return;
         GameStopped = true;
        PlayButton.SetActive(true);
        if(StartDelayText.gameObject.activeSelf)StartDelayText.gameObject.SetActive(false);
            if (StartDelayCoroutine!=null)StopCoroutine(StartDelayCoroutine);
        // StopAllCoroutines();
        // StopCoroutine()
    }
        void OnApplicationFocus(bool hasFocus)
    {   if(hasFocus) return;
        if((!finScreen.activeSelf&&!MovementPath.final)){
            PlayerPrefs.SetInt("inProgress",1);
            SaveLevelProgress();
            
          pause();
        
        }else{
            if(MovementPath.final){
                TryIncAndSave();
            }
            PlayerPrefs.SetInt("inProgress",0);
            
            //  Debug.Log("ne save  ");
        }
        SM.SaveScore();
        // Debug.Log("Application focus  " + hasFocus);
    }

    public void RestartWithSettings(){
        valuesChange();
        settingsGo.SetActive(false);
        StartLVL();
    }
    public void StartLVL(){
        if(finScreen.activeSelf) finScreen.SetActive(false);
        GameStopped = false;
        Destroy(pathGO1);
        PlayerPrefs.SetInt("inProgress",0);
        if(StartDelayText.gameObject.activeSelf)StartDelayText.gameObject.SetActive(false);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, $"Start level {level}");

    }

    public void Settings(){

        if (!settingsGo.activeSelf){
            GameStopped = true;
            if (StartDelayCoroutine!=null)StopCoroutine(StartDelayCoroutine);
            settingsGo.SetActive(true);
            settingsGo.GetComponent<settings>().GetSettings();
             if(StartDelayText.gameObject.activeSelf)StartDelayText.gameObject.SetActive(false);
        }else{
            valuesChange();
            StartDelayCoroutine=StartCoroutine(StartDelay());
            settingsGo.SetActive(false);
        
        }
        
    }
    public void valuesChange(){
         settingsGo.GetComponent<settings>().SetSettings();
    }

    void SaveGame(){
            SM.SaveScore();
        GameData dat = new GameData(level,mapNum);
        string strData = JsonUtility.ToJson(dat);
        PlayerPrefs.SetString("GameData",strData);
        PlayerPrefs.Save();
    }
    void SaveLevelProgress(){
        if( path.chain==null)return;
        
        MovementPath.OneChain chn = path.chain;
        
        List<int> pathValues = new List<int>();
        int len = chn.length();
        for (int i = 0; i < len; i++)
        {
            pathValues.Add(chn.GetBall(i).GetComponent<Ball>().value);
        }
        List<int> cannonValues = new List<int>();
        for (int i = 0; i <2; i++)
        {
            cannonValues.Add(cannon.Balls[i].GetComponent<Ball>().value);
        }
        float head = MovementPath.GOBack? len*path.ballRadius :chn.headDist;

         GameData.PathProgress dat = new GameData.PathProgress(head,pathValues,cannonValues,MovementPath.GOBack);
        string strData = JsonUtility.ToJson(dat);
        PlayerPrefs.SetString("PathProgress",strData);
        PlayerPrefs.Save();
    }

    void LoadGame(){
        if(PlayerPrefs.HasKey("GameData")){
            string strData = PlayerPrefs.GetString("GameData");
            GameData dat = JsonUtility.FromJson<GameData>(strData);
            level = dat.lvlNum;
            mapNum = dat.mapNum;
        }
    }
    public void LoadLevelProgress(){
        if(PlayerPrefs.HasKey("PathProgress")&&PlayerPrefs.HasKey("inProgress")&&PlayerPrefs.GetInt("inProgress")==1){
            string strData = PlayerPrefs.GetString("PathProgress");
            GameData.PathProgress dat = JsonUtility.FromJson< GameData.PathProgress>(strData);
            path.chainHead =dat.headDist;
            spawner.SpawnSavedBalls(dat.headDist,dat.valuesOnPath);

            if(dat.IsOnStart){
            int len = dat.valuesOnPath.Count;
                spawner.prevVaule = dat.valuesOnPath[len-1];
                spawner.SpawnBalls(BallsNumber-len);
            }
            cannon.SetBallsInCannon(dat.valuesInCannon);
            
            PlayerPrefs.Save();
        }
    }
        IEnumerator StartDelay(){
            // float sec = 3f;
            StartDelayText.gameObject.SetActive(true);
            // StartDelayText.text="3";
            // yield return new WaitForSeconds(sec);
            // StartDelayText.text="2";
            // yield return new WaitForSeconds(sec);
            // StartDelayText.text="1";
            // yield return new WaitForSeconds(3);
            // StartDelayText.text="";
            //  StartDelayText.gameObject.SetActive(false);
            // GameStopped=false;
            yield return new WaitForSeconds(3.5f);
            
            
            path.CheckAllBalls();

            
        }

    public void Revert(){
       PlayerPrefs.DeleteAll();
        mapNum = 1;
        level = 1;
        SM.RevertScore();
        SceneManager.LoadScene("Game");

    }
    
    public void SummonPuf(Transform ball){
       Destroy( Instantiate(Puf,ball.position,Quaternion.identity,ball),1);
    }
    
        public void SummonFinalPuf(Transform ball){
            GameObject particl = Instantiate(FinalPuf,ball.position,Quaternion.Euler(Vector3.left*90),ball);
            particl.transform.localScale=Vector3.one*path.ballRadius;
    }

}
