﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DG.Tweening;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;
using UnityEngine.UI;

public class MovementPath : MonoBehaviour
{
    public List<Transform> ballList;
   [SerializeField]float speed;
   public float ballRadius ;
   [SerializeField]float FastDuration;
   [SerializeField]float RegularDuration;
       private int addBallIndex;
	[SerializeField] Ease easeType;
    [SerializeField] Ease AddBallEase ;
    GameManager GM;
    ScoreManager SM;
    public float finalFactor;
    public float GoBackFactor;
    public OneChain chain;
    public class OneChain {
        public Transform head(){
            return chainObj.transform.GetChild(0).transform;
        }
       public Transform end(){
            return chainObj.transform.GetChild(length()-1).transform;
        }
        public float headDist;
        public bool IsMoving;
        public GameObject chainObj;
        public OneChain(List<Transform> balls,float dist, Transform chainParent){
            headDist = dist;
            chainObj = new GameObject();
            chainObj.transform.parent = chainParent;
            foreach (var ball in balls)
            {
                ball.parent= chainObj.transform;
            }

            chainObj.transform.name = "chn"+chainObj.transform.GetSiblingIndex();
        }
        public int length(){
            return chainObj.transform.childCount;
        }
        public Transform GetBall(int i){
                
                return chainObj.transform.GetChild(i).transform;
        }
        public List<Transform> GetBalls(int from, int to){
            List<Transform> childs = new List<Transform>();
             for (int i = from; i <= to; i++)
             {
                 childs.Add(GetBall(i));
             }
            return childs;
        }

        public List<Transform> GetAllBalls(){
            List<Transform> childs = new List<Transform>();
             for (int i = 0; i < length(); i++)
             {
                 childs.Add(GetBall(i));

             }
            return childs;
        }
    }

    public static bool GOBack = false;   
    public static bool final = false;
    public int matchCounter;
    int lvlspeed;
   
    bool IsOnStart;
    public float chainHead;
    void Awake(){
        PathWidthScale();
    }
    void Start(){
		addBallIndex = -1;
        lvlspeed=1;
        GOBack=false;
        GM = GetComponentInParent<GameManager>();
        SM = GetComponentInParent<ScoreManager>();
        chainHead = 0;
 
    }

    void Update(){
        if(GameManager.GameStopped) return;
        if (speed != GM.BallsSpeed*0.1f) speed = GM.BallsSpeed*0.1f;

        if(final&&matchCounter!=0)matchCounter=0;

        AddBallsToChain();
        if (chain==null)return;
        IsOnStart = chain.headDist<=(chain.length()-1)*ballRadius;
		if (!GOBack){
            
			MoveAllBallsAlongPath();
            }
       if(matchCounter>=3&&!IsOnStart&&!final) GOBack = true;

        if(GOBack&&!final){
            ChainGoBack(chain);
            if (IsOnStart) {
                GOBack=false;
                GetComponent<BallSpawner>().prevVaule = chain.end().GetComponent<Ball>().value;
                GetComponent<BallSpawner>().SpawnBalls(GM.BallsNumber-chain.length());
                }
        }
    }
    void AddBallsToChain(){
        List<Transform> balls = new List<Transform>();
        for (int i = 0; i < transform.childCount; i++)
        {
            if(transform.GetChild(i).tag== "ActiveBalls") balls.Add(transform.GetChild(i));
        }
        if (balls.Count>0) {
        
            if(chain==null){
                  chain = new OneChain(balls,chainHead,transform);
                
                 chain.IsMoving = true;
                 
                }else {
                    foreach (var ball in balls)
                    {
                        ball.parent = chain.chainObj.transform;
                    }
                }

        }
    }


private void MoveAllBallsAlongPath()
	{

            if(chain.IsMoving){
                Vector3 tangent;
                float spd = speed*lvlspeed;
                    if(IsOnStart)spd= speed*GoBackFactor;
                    if(final)spd= speed*finalFactor;

                chain.headDist += spd * Time.deltaTime;

                for (int i = 0; i < chain.length(); i++)
                {
                    float currentBallDist = chain.headDist - i * ballRadius;
                    Vector3 pos = GetComponent<BGCcMath>().CalcPositionAndTangentByDistance(currentBallDist , out tangent);
                    if(final) {
                        chain.GetBall(i).DOMove(pos, FastDuration);
                        break;
                    }
                    if (i == addBallIndex && addBallIndex != -1)
                        chain.GetBall(i).DOMove(pos, IsOnStart ?FastDuration:RegularDuration).SetEase(AddBallEase);
                    else
                        chain.GetBall(i).DOMove(pos,IsOnStart ?FastDuration:RegularDuration).SetEase(easeType);;

                    chain.GetBall(i).GetComponent<Ball>().tangent = tangent;
                }


        }
		
	}
	public void AddNewBallAt(GameObject go, int index, int touchedBallIdx)
	{
        if(chain==null)return;
		addBallIndex = index;

		go.transform.parent = chain.chainObj.transform;
		go.transform.SetSiblingIndex(index);

                chain.headDist += 0.5f*ballRadius;
 
        StartCoroutine(MatchCheckDelay(0.45f,chain.GetBall(index)));
	}

    void MatchCheck(Transform ball){
       if(ball == null||GameManager.GameStopped)return;
       addBallIndex = -1;
        int i = ball.GetSiblingIndex();

        int thisvalue = chain.GetBall(i).GetComponent<Ball>().value;
        int nextval = i<chain.length()-1?chain.GetBall(i+1).GetComponent<Ball>().value:-1;
        int prevval = i>0?chain.GetBall(i-1).GetComponent<Ball>().value:-1;
        int MatchDir=0;
        List<int> sameValues = new List<int>();
        sameValues.Add(i);

        if (nextval == prevval&& thisvalue==prevval){
            MatchDir = GetMatchDirection(i);
        }

        if((MatchDir==0||MatchDir==-1)&&i>0&&chain.GetBall(i-1).GetComponent<Ball>().value == thisvalue){
            sameValues.Add(i-1);
        }
        
       if((MatchDir==0||MatchDir==1)&&i<chain.length()-1&&sameValues.Count==1&&chain.GetBall(i+1).GetComponent<Ball>().value == thisvalue){
            sameValues.Add(i+1);
        }

        sameValues.Sort();
        int beforeMatchCounter = matchCounter;
        //если матчитася
        if (sameValues.Count>1){
            SM.incScore(thisvalue);
            int BeforeSplitLenght = chain.length();

            List<Transform> MatchedBalls = chain.GetBalls(sameValues[0],sameValues[sameValues.Count-1]);

            if (!sameValues.Contains(BeforeSplitLenght-1))
            {
                 chain.headDist -= ballRadius;
            } 

             ReplaceMatched(MatchedBalls);
            matchCounter++;
        }
        if(beforeMatchCounter==matchCounter) matchCounter= 0;
       
    }

    int GetMatchDirection(int i){
        int thisval = chain.GetBall(i).GetComponent<Ball>().value;
        int BeforeCount= 0;
        int AfterCount= 0;
                int j=i+2;
                while (j<chain.length()&&chain.GetBall(j).GetComponent<Ball>().value == thisval+Mathf.Abs(i-j)-1)
                {
                    AfterCount++;
                    j++;
                }
                j=i-2;
                while (j>0&&chain.GetBall(j).GetComponent<Ball>().value == thisval+Mathf.Abs(i-j)-1)
                {
                    BeforeCount++;
                    j--;
                }
                
            

        return BeforeCount>=AfterCount?-1:1;
    }

    void ChainGoBack(OneChain chn){
		Vector3 tangent;
		chn.headDist -= speed * GoBackFactor * Time.deltaTime;

        for (int i = 0; i < chn.length(); i++)
        {
            float currentBallDist = chn.headDist - i * ballRadius;
			Vector3 pos = GetComponent<BGCcMath>().CalcPositionAndTangentByDistance(currentBallDist , out tangent);

				chn.GetBall(i).DOMove(pos, FastDuration).SetEase(easeType);;

            chn.GetBall(i).GetComponent<Ball>().tangent = tangent;
        }
    }

        IEnumerator MatchCheckDelay(float sec, Transform ball){
            yield return new WaitForSeconds(sec);
            MatchCheck(ball);
        }
        public void CheckAllBalls(){
            List<Transform> balls = chain.GetAllBalls();
            foreach(var ball in balls){
                MatchCheck(ball);
                if (matchCounter>0)break;
            }
        }

        public void ReplaceMatched(List<Transform> balls){
            

                if(balls[0].gameObject != null){
                balls[0].DOKill();
                balls[0].gameObject.tag = "Matched";
                Destroy(balls[0].gameObject);   
                }

                if(balls[1].gameObject != null){
                balls[1].GetComponent<Ball>().value++;
                balls[1].GetComponent<Ball>().UpdValue();
                GM.SummonPuf(balls[1]);
                }    
            if(balls[1].GetComponent<Ball>().value != GM.powOfWin+GM.level){
                //  FinalMatching(balls[1]);
                 StartCoroutine(MatchCheckDelay(0.3f,balls[1]));
                 }else{
                     FinalMatching(balls[1]);
                 }
                 
        }

        void FinalMatching(Transform finBall){
            DOTween.KillAll();
            chain.headDist -= finBall.GetSiblingIndex()*ballRadius;
            foreach (var ball in chain.GetAllBalls())
            {
                if(ball!=finBall) {
                    Destroy(ball.gameObject);
                }
            }
            
            final = true;
            GOBack=false;
            // GM.SummonFinalTrail(finBall);
        }

        void PathWidthScale(){
            ballRadius*=GameScaler.factor;
            
            GetComponent<LineRenderer>().widthMultiplier=ballRadius;
            GetComponent<LineRenderer>().UpdateGIMaterials();        }
}
