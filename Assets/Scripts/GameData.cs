﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public int lvlNum=1;
    public int mapNum=1;

    public GameData(int lvl,int map){
        lvlNum = lvl;
        mapNum = map; 
    }
    public class PathProgress{
        public float headDist;
        public bool IsOnStart;
        public List<int> valuesOnPath = new List<int>();
        public List<int> valuesInCannon = new List<int>(2);

        public PathProgress(float head, List<int> PathBalls,List<int> CannonBalls,bool isOnStart){
            headDist = head;
            valuesInCannon = CannonBalls;
            valuesOnPath= PathBalls;
            IsOnStart=isOnStart;
        }
    }
     public class ScoreData{
         public int _lvl;
         public int _exp;
         public int _score;
         public int _highscore;

         public ScoreData(int lvl,int exp,int score,int highscore){
             _lvl=lvl;
             _exp = exp;
             _score = score;
             _highscore = highscore;
         }
     }

}
